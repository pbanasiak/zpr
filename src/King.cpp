#include "King.h"

King::King(int newX, int newY, PlayerColor c) :
		Piece(newX, newY, c)
{

}
JSONPiece King::getJSON()
{
	JSONPiece json;
	json.x = cords.x;
	json.y = cords.y;
	if (color == WHITE)
	{
		json.color = 'w';
	}
	else
	{
		json.color = 'b';
	}
	json.type = 'k';
	return json;
}
bool King::isKing()
{
	return true;
}
