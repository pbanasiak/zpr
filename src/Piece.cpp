#include "Piece.h"

Piece::Piece(int newX, int newY, PlayerColor c)
{
	cords = Cords(newX, newY);
	color = c;
}

bool Piece::isKing()
{
	return false;
}
JSONPiece Piece::getJSON()
{
	JSONPiece json;
	json.x = cords.x;
	json.y = cords.y;
	if (color == WHITE)
	{
		json.color = 'w';
	}
	else
	{
		json.color = 'b';
	}
	json.type = 'n';
	return json;
}
void Piece::move(Cords c)
{
	cords = c;
}
bool Piece::isPromoting()
{
	if (color == WHITE)
	{
		if (cords.y == 0)
		{
			return true;
		}
	}
	else if (color == BLACK)
	{
		if (cords.y == 7)
		{
			return true;
		}
	}
	return false;
}
