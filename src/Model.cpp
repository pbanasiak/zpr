#include "Model.h"

int Model::boardSize = 8;
int Model::piecesRows = 2;

Model::Model()
{
	pieces = std::vector<Piece*>();
	initialize();
}

Model::~Model()
{
	for (Piece* p : pieces)
	{
		delete p;
	}
}
void Model::initialize()
{
	playerTurn = 0;
	std::string c;
	players[1] = c;
	players[0] = c;
	for (int i = 0; i < boardSize; i++)
	{
		for (int j = 0; j < piecesRows; j++)
		{
			if ((i + j) % 2 == 1)
			{
				pieces.push_back(new Piece(i, j, BLACK));
			}
		}
	}
	for (int i = 0; i < boardSize; i++)
	{
		for (int j = boardSize - piecesRows; j < boardSize; j++)
		{
			if ((i + j) % 2 == 1)
			{
				pieces.push_back(new Piece(i, j, WHITE));
			}
		}
	}
	turn = WHITE;
}

ptree Model::getJSON()
{

	ptree pt;
	pt.put("messageID", 1);
	ptree children;
	std::vector < ptree > childVector;
	ptree child;
	JSONPiece j;
	for (Piece* p : pieces)
	{
		childVector.push_back(ptree());
		child = childVector.back();
		j = p->getJSON();
		child.put("x", j.x);
		child.put("y", j.y);
		child.put("color", j.color);
		child.put("type", j.type);
		children.push_back(std::make_pair("", child));
	}
	pt.add_child("Array", children);
	return pt;
}
int Model::find(Cords c)
{
	int i = 0;
	for (auto it : pieces)
	{
		if (it->cords == c)
		{
			return i;
		}
		i++;
	}
	return -1;
}
PlayerColor Model::oppositeColor(PlayerColor c)
{
	if (c == WHITE)
	{
		return BLACK;
	}
	else
	{
		return WHITE;
	}
}
void Model::changeTurn()
{
	turn = oppositeColor(turn);
}
bool Model::click(Cords begin, Cords end)
{
	if (end == begin)
	{
		return false;
	}
	int position = find(begin);
	Piece* p;
	if (position != -1)
	{
		p = pieces[position];
		if (p->color != turn)
		{
			std::cout << "Not your turn" << std::endl;
			return false;
		}
		if (canAttack())
		{
			if (isLegalAttack(*p, begin, end))
			{
				attack(*p, end);
				if (!canAttack())
				{
					changeTurn();
					return true;
				}
			}
		}
		else if (isLegalMove(*p, begin, end))
		{
			move(*p, end);
			changeTurn();
			return true;
		}
		return false;
	}
	else
	{
		std::cout << "blad, nie ma takiego pionka" << std::endl;
		return false;
	}
}
void Model::move(Piece& p, Cords end)
{
	p.move(end);
	if (!p.isKing())
	{
		if (p.isPromoting())
		{
			pieces.erase(pieces.begin() + find(p.cords));
			pieces.push_back(new King(p.cords.x, p.cords.y, p.color));
		}
	}
}
bool Model::hasWon()
{
	return hasWon(WHITE) || hasWon(BLACK);
}

bool Model::hasWon(PlayerColor p)
{
	for (unsigned int i = 0; i < pieces.size(); i++)
	{
		if (pieces[i]->color == oppositeColor(p))
		{
			return false;
		}
	}
	return true;
}
void Model::attack(Piece& p, Cords end)
{
	std::vector<Cords> between = end.between(p.cords);
	int position;
	for (Cords c : between)
	{
		position = find(c);
		if (position != -1)
		{
			pieces.erase(pieces.begin() + position);
		}
	}
	move(p, end);
}
bool Model::isLegalMove(Piece& p, Cords begin, Cords end)
{
	if (!p.isKing())
	{
		if (begin.distance(end) == 1)
		{
			if ((p.color == WHITE && begin.y > end.y)
					|| (p.color == BLACK && begin.y < end.y))
			{
				if (find(end) == -1)
				{
					return true;
				}
			}
		}
		return false;
	}
	else if (begin.distance(end) > 0)
	{
		for (Cords c : begin.between(end))
		{
			if (find(c) != -1)
			{
				return false;
			}
		}
		return true;
	}
	return false;
}
bool Model::isLegalAttack(Piece& p, Cords begin, Cords end)
{
	if (!p.isKing())
	{
		return isLegalAttackPiece(p, begin, end);
	}
	else
	{
		return isLegalAttackKing(p, begin, end);
	}
}
bool Model::isLegalAttackPiece(Piece& p, Cords begin, Cords end)
{
	if (begin.distance(end) == 2)
	{
		if (find(end) == -1)
		{
			std::vector<Cords> test = end.between(p.cords);
			Cords between = test.back();
			int position = find(between);
			if (position != -1
					&& pieces[position]->color == oppositeColor(turn))
			{
				return true;
			}
		}
	}
	return false;
}
bool Model::isLegalAttackKing(Piece& p, Cords begin, Cords end)
{
	if (begin.distance(end) > 1 && find(end) == -1
			&& !end.between(p.cords).empty()) //jest jakas odleglosc
	{
		std::vector<Cords> test = end.between(p.cords);
		for (Cords c : test)
		{
			int position = find(c);
			if (position != -1 && pieces[position]->color == turn)
			{
				return false;
			}
		}
		bool result = false;
		for (Cords c : test)
		{
			int position = find(c);
			if (position != -1
					&& pieces[position]->color == oppositeColor(turn))
			{
				std::vector < Cords > test2 = end.between(c);
				for (Cords c2 : test2)
				{
					if (find(c2) != -1)
					{
						return false;
					}
				}
				result = true;
			}
		}
		return result;
	}

	return false;
}
bool Model::hasPossibleAttack(Piece& p)
{
	std::vector<Cords> neighbours;
	if (!p.isKing())
	{
		neighbours = p.cords.getPlaceAfterJump();
	}
	else
	{
		neighbours = p.cords.getPlaceAfterJumpKing();
	}
	for (Cords c : neighbours)
	{
		if (isLegalAttack(p, p.cords, c))
		{
			return true;
		}
	}
	return false;
}
bool Model::canAttack()
{
	for (Piece* p : pieces)
	{
		if (p->color == turn)
		{
			if (hasPossibleAttack(*p))
			{
				std::cout << "MOZLIWE BICIE" << std::endl;
				return true;
			}
		}
	}
	return false;
}

std::vector<Piece*> Model::getPieces()
{
	return pieces;
}
