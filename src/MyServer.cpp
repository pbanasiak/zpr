//#define BOOST_LOG_DYN_LINK
#include "MyServer.h"

using namespace std;
//namespace logging = boost::log;
//using namespace logging::trivial;

MyServer::MyServer()
{
	//Inicjacja Asio transport
	rooms = std::vector<Model*>();
	playersToRoom = std::map<const char*, int>();
	nicks = std::set<std::string>();
	createRoom();
	m_server.init_asio();
	//handler callbacks
	m_server.set_open_handler(bind(&MyServer::on_open, this, ::_1));
	m_server.set_close_handler(bind(&MyServer::on_close, this, ::_1));
	m_server.set_message_handler(bind(&MyServer::on_message, this, ::_1, ::_2));
	m_server.set_access_channels(websocketpp::log::alevel::none);
}

void MyServer::createRoom()
{
	rooms.push_back(new Model());
}
void MyServer::on_open(connection_hdl hdl)
{
	std::cout << "Polaczono" << std::endl;
	unique_lock < mutex > lock(m_action_lock);
	m_actions.push(action(CONNECTED, hdl));
	lock.unlock();
	m_action_cond.notify_one();

}

void MyServer::on_close(connection_hdl hdl)
{
	std::cout << "Zamknieto" << std::endl;
	unique_lock < mutex > lock(m_action_lock);
	m_actions.push(action(UNCONNECTED, hdl));
	lock.unlock();
	m_action_cond.notify_one();
}

void MyServer::on_message(connection_hdl hdl, server::message_ptr msg)
{
	std::cout << "Dostano wiadomosc" << std::endl;
	unique_lock < mutex > lock(m_action_lock);
	m_actions.push(action(MESSAGE, hdl, msg));
	lock.unlock();
	m_action_cond.notify_one();
}

void MyServer::run(uint16_t port)
{
	m_server.listen(port);
	std::cout << "Nasluchuje na port " << port << std::endl;
	m_server.start_accept();
	try
	{
		m_server.run();
	} catch (const std::exception & e)
	{
		std::cout << e.what() << std::endl;
	} catch (websocketpp::lib::error_code e)
	{
		std::cout << e.message() << std::endl;
	} catch (...)
	{
		std::cout << "other exception" << std::endl;
	}

}

const char* MyServer::parseAdress(connection_hdl hdl)
{
	const char* s = (const char*) (hdl.lock().get());
	return s;
}
void MyServer::sendBoard(int room)
{
	for (connection_hdl it : m_connections)
	{
		if (playersToRoom.find(parseAdress(it))->second == room)
		{
			std::cout << "room" << std::endl;
			stringstream ss;
			std::vector<Model*>::iterator mit = rooms.begin() + room;
			Model* m = *mit;
			write_json(ss, m->getJSON());
			ptree pt;
			std::string s(ss.str());
			m_server.send(it, s, websocketpp::frame::opcode::text);
		}
	}
}
void MyServer::process_message()
{
	while (1)
	{
		unique_lock < mutex > lock(m_action_lock);

		while (m_actions.empty())
		{
			m_action_cond.wait(lock);
		}
		while (m_actions.size() == 0)
			;
		action a = m_actions.front();
		m_actions.pop();

		lock.unlock();

		if (a.type == CONNECTED)
		{
			unique_lock < mutex > con_lock(m_connection_lock);
			m_connections.insert(a.hdl);
		}
		else if (a.type == UNCONNECTED)
		{
			unique_lock < mutex > con_lock(m_connection_lock);

			m_connections.erase(a.hdl);
			if (playersToRoom.find(parseAdress(a.hdl)) != playersToRoom.end())
			{
				int room = playersToRoom.find(parseAdress(a.hdl))->second;
				endGame(room, 2);
				playersToRoom.erase(parseAdress(a.hdl));
			}
		}
		else if (a.type == MESSAGE)
		{
			unique_lock < mutex > con_lock(m_connection_lock);
			process_json(a.hdl, a.msg);
		}
	}
}
void MyServer::sendCommunicate(std::string text, int room)
{
	for (connection_hdl it : m_connections)
	{
		if (playersToRoom.find(parseAdress(it))->second == room)
		{
			stringstream ss;
			ptree pt;
			pt.put("messageID", 3);
			pt.put("name", "GAME");
			pt.put("text", text);
			write_json(ss, pt);
			std::string s(ss.str());
			m_server.send(it, s, websocketpp::frame::opcode::text);
		}
	}
}
int MyServer::howManyPlayers(int room)
{

	int result = 0;
	for (connection_hdl it : m_connections)
	{
		if (playersToRoom.find(parseAdress(it)) != playersToRoom.end())
		{
			if (playersToRoom.find(parseAdress(it))->second == room)
			{
				result++;
			}
		}
	}
	return result;
}
void MyServer::endGame(int room, int winner)
{

	connection_hdl player1;
	connection_hdl player2;
	for (connection_hdl it : m_connections)
	{
		if (playersToRoom.find(parseAdress(it))->second == room)
		{
			stringstream ss;
			ptree pt;
			pt.put("messageID", 7);
			pt.put("winner", winner);
			write_json(ss, pt);
			std::string s(ss.str());
			m_server.send(it, s, websocketpp::frame::opcode::text);
			player1 = it;
			player2 = it;
		}
	}
	std::vector<Model*>::iterator it = rooms.begin() + room;
	Model* m = *it;
	m->initialize();
	playersToRoom.erase(parseAdress(player1));
	playersToRoom.erase(parseAdress(player2));

}
void MyServer::parseClick(ptree& pt2)
{
	int room = pt2.get<int>("room");
	std::string player = pt2.get < std::string > ("name");
	std::vector<Model*>::iterator it = rooms.begin() + room;
	Model* m = *it;
	switch (m->playerTurn)
	{
	case WHITE:
		sendCommunicate("Tura gracza - BIALE", room);
		break;
	case BLACK:
		sendCommunicate("Tura gracza - CZARNE", room);
		break;

	}
	int xBefore = pt2.get<int>("xBefore");
	int yBefore = pt2.get<int>("yBefore");
	int xAfter = pt2.get<int>("xAfter");
	int yAfter = pt2.get<int>("yAfter");
	Cords before(xBefore, yBefore);
	Cords after(xAfter, yAfter);
	if ((m->players[1] == player && m->playerTurn == 1)
			|| (m->players[0] == player && m->playerTurn == 0))
	{
		if (m->click(before, after) == true)
		{
			m->playerTurn = (m->playerTurn + 1) % 2;
			if (m->hasWon())
			{
				int winner = m->hasWon(BLACK) ? 1 : 0;
				endGame(room, winner);
			}
		}
		else
		{
			sendCommunicate("Nielegalny ruch", room);
		}
	}
	sendBoard(room);
}
void MyServer::parseList()
{

	int tab[rooms.size()];
	for (unsigned int i = 0; i < rooms.size(); i++)
	{
		tab[i] = howManyPlayers(i);
	}
	for (connection_hdl it : m_connections)
	{
		stringstream ss;
		ptree pt;
		ptree children;
		pt.put("messageID", 4);
		std::vector < ptree > childVector;
		ptree child;
		for (unsigned int i = 0; i < rooms.size(); i++)
		{
			childVector.push_back(ptree());
			child = childVector.back();
			child.put("numberOfRoom", i);
			child.put("numberOfPlayers", tab[i]);	//tego jeszcze nie umiem
			children.push_back(std::make_pair("", child));
		}
		pt.add_child("Array", children);
		write_json(ss, pt);
		std::string s(ss.str());
		m_server.send(it, s, websocketpp::frame::opcode::text);
	}
}

void MyServer::parseChat(ptree& pt2)
{
	std::string player = pt2.get < std::string > ("name");
	std::string text = pt2.get < std::string > ("text");
	int room = pt2.get<int>("room");
	for (connection_hdl it : m_connections)
	{
		if (playersToRoom.find(parseAdress(it))->second == room)
		{
			stringstream ss;
			ptree pt;
			pt.put("messageID", 3);
			pt.put("name", player);
			pt.put("text", text);
			write_json(ss, pt);
			std::string s(ss.str());
			m_server.send(it, s, websocketpp::frame::opcode::text);
		}
	}
}
void MyServer::parseNicks(std::string name, int room, connection_hdl hdl)
{
	ptree pt;
	stringstream ss;
	pt.put("messageID", 2);
	if (nicks.count(name) == 1)
	{
		pt.put("canEnter", 1);
	}
	else if (howManyPlayers(room) > 1)
	{
		pt.put("canEnter", 2);
	}
	else
	{
		pt.put("canEnter", 0);
		nicks.insert(name);
	}
	write_json(ss, pt);
	std::string s(ss.str());
	m_server.send(hdl, s, websocketpp::frame::opcode::text);

}
void MyServer::process_json(connection_hdl hdl, server::message_ptr& msg)
{
	ptree pt2;
	string s = msg->get_payload();
	string s2;
	stringstream ss;
	stringstream ss2;
	ss << s;
	read_json(ss, pt2);
	int messageID = pt2.get<int>("messageID");

	int room;
	std::string player;
	std::pair<const char*, int> para;
	ptree pt;
	switch (messageID)
	{
	case 1:
		parseClick(pt2);
		break;
	case 2:
		player = pt2.get < std::string > ("name");
		room = pt2.get<int>("room");
		parseNicks(player, room, hdl);
		break;
	case 3:
		parseChat(pt2);
		break;
	case 4:
		parseList();
		break;
	case 5:
		createRoom();
		parseList();
		break;
	case 6:
		room = pt2.get<int>("room");
		para = std::pair<const char*, int>(parseAdress(hdl), room);
		playersToRoom.insert(para);
		player = pt2.get < std::string > ("name");
		std::vector<Model*>::iterator it = rooms.begin() + room;
		Model* m = *it;
		if (m->players[0].size() == 0)
		{
			m->players[0] = player;
			sendCommunicate("Biale - " + player, room);
		}
		else if (m->players[1].size() == 0 && m->players[0] != player)
		{
			m->players[1] = player;
			sendCommunicate("Czarne - " + player, room);
		}
		sendBoard(room);
		break;
	}
}
void init()
{
	/*logging::add_file_log("sample.log");
	 logging::core::get()->set_filter(
	 logging::trivial::severity >= logging::trivial::info);
	 BOOST_LOG_TRIVIAL(trace) << "AN error severity message";*/
}
int main()
{
	try
	{
		MyServer server;

		thread t(bind(&MyServer::process_message, &server));

		server.run(9002);

		t.join();

	} catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}
}
