#include "Cords.h"

Cords::Cords(int newX, int newY)
{
	x = newX;
	y = newY;
}

Cords::~Cords()
{
}

std::string Cords::message()
{
	return x + "," + y;
}
bool Cords::operator==(const Cords& c)
{
	return c.x == x && c.y == y;
}
int Cords::distance(Cords end)
{
	if (abs(end.y - y) == abs(end.x - x))
	{
		return abs(end.y - y);
	}
	return -1;
}

std::vector<Cords> Cords::between(Cords end)
{
	std::vector < Cords > between = std::vector<Cords>();
	for (int i = 1; i < abs(x - end.x); i++)
	{
		if (end.x > x)
		{
			if (end.y > y)
			{
				between.push_back(Cords(x + i, y + i));
			}
			else
			{
				between.push_back(Cords(x + i, y - i));
			}
		}
		else
		{
			if (end.y > y)
			{
				between.push_back(Cords(x - i, y + i));
			}
			else
			{
				between.push_back(Cords(x - i, y - i));
			}
		}
	}
	return between;
}

std::vector<Cords> Cords::getPlaceAfterJump()
{
	std::vector<Cords> neighbours = std::vector<Cords>();
	int newX, newY;
	for (int i = -2; i <= 2; i++)
	{
		for (int j = -2; j <= 2; j++)
		{
			newX = x + i;
			newY = y + j;
			if (newX >= 0 && newY >= 0 && newX <= 7 && newY <= 7)
			{
				neighbours.push_back(Cords(newX, newY));
			}
		}
	}
	return neighbours;
}
std::vector<Cords> Cords::getPlaceAfterJumpKing()
{
	std::vector<Cords> neighbours = std::vector<Cords>();
	int newX, newY;
	for (int i = -7; i <= 7; i++)
	{
		for (int j = -7; j <= 7; j++)
		{
			newX = x + i;
			newY = y + j;
			if (newX >= 0 && newY >= 0 && newX <= 7 && newY <= 7
					&& abs(i) == abs(j) && abs(i) >= 2)
			{
				neighbours.push_back(Cords(newX, newY));
			}
		}
	}
	return neighbours;
}
