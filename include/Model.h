#pragma once
#include "Cords.h"
#include "PlayerColor.h"
#include "Piece.h"
#include "King.h"
#include <vector>
#include <iostream>
#include <sstream>

/**
 * Klasa modelu, obsluguje cala logike gry w warcaby
 *
 */
class Model
{
public:
	/**
	 * Standardowy konstruktor modelu
	 */
	Model(void);
	/**
	 * Destruktor modelu
	 */
	~Model(void);
	/**
	 * Ustawia pionki na poczatkowych pozycjach w tablicy
	 */
	void initialize();
	//bool attack(Cords);
	/**
	 * Sprawdza poprawnosc klikniecia przekazanego od klienta,
	 * nastepnie sprawdza poprawnosc wykonanej akcji
	 */
	bool click(Cords, Cords);
	/**
	 * Parsowanie komunikatu JSON
	 * @param begin wspolrzedne poczatkowe
	 * @param end wspolrzedne koncowe
	 */
	ptree getJSON();
	/**
	 * Poruszanie pionkiem z jednego miejsca na drugie
	 */
	void move(Piece&, Cords);
	/**
	 * Funkcja atakujaca, przy jej uzyciu mamy pewnosc ze ruch jest mozliwy i poprawny
	 */
	void attack(Piece&, Cords);
	/**
	 * Sprawdza czy zadany ruch jest mozliwy
	 * @param p dany pionek
	 * @param end wspolrzedne konca ruchu
	 */
	bool isLegalMove(Piece&, Cords, Cords);
	/**
	 * Sprawdza, czy mozna atakowac, wywoluje odpowiednia funkcje dla
	 * danego rodzaju pionka
	 * @param p dany pionek
	 * @param begin wspolrzedne poczatkowe
	 * @param end wspolrzedne koncowe
	 */
	bool isLegalAttack(Piece&, Cords, Cords);
	/**
	 * Sprawdza legalnosc ataku pionka
	 * @param p dany pionek
	 * @param begin wspolrzedne poczatkowe
	 * @param end wspolrzedne koncowe
	 */
	bool isLegalAttackPiece(Piece&, Cords, Cords);
	/**
	 * Sprawdza legalnosc ataku dla damki
	 * @param p dana damka
	 * @param begin wspolrzedne poczatkowe
	 * @param end wspolrzedne koncowe	 */
	bool isLegalAttackKing(Piece&, Cords, Cords);
	/**
	 * Sprawdza czy dany pionek ma mozliwosc atakowania
	 * @param p pionek ktory sprawdzamy
	 */
	bool hasPossibleAttack(Piece&);
	/**
	 * Sprawdza czy istnieje pionek ktory moze atakowac
	 */
	bool canAttack();
	/**
	 * Znajduje pionek o zadanych wspolrzednych
	 * @param c wspolrzedne
	 */
	int find(Cords);

	/**
	 * Czy gracz wygral
	 */
	bool hasWon(PlayerColor);

	/**
	 * czy wygralktokolwiek
	 */
	bool hasWon();

	/**
	 * Zmiena ture na przeciwnika
	 */
	void changeTurn();
	/**
	 * Zwraca kolor przeciwny do zadanego
	 * @param c zadany kolor
	 */
	PlayerColor oppositeColor(PlayerColor);
	/**
	 * Przechowuje nazwy gracyz
	 */
	std::string players[2];
	/**
	 * Przechowuje ture uzytkownika
	 */
	int playerTurn;
	/**
	 * Getter do wektora pionkow
	 */
	std::vector<Piece*> getPieces();
private:
	/**
	 * Wektor pionkow
	 */
	std::vector<Piece*> pieces;
	/**
	 * Kolor uzytkownika czyjego jest tura
	 */
	PlayerColor turn;
	/**
	 * Rozmiar planszy
	 */
	static int boardSize;
	/**
	 * Ilosc poczatkowych rzedow
	 */
	static int piecesRows;
};

