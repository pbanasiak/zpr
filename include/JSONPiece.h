/**
 * Struktura JSON
 */
struct JSONPiece
{
	int x;
	int y;
	char color;
	char type;
};
