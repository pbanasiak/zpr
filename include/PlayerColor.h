#pragma once
/**
 * Typ wyliczeniowy dla kolorow pionkow
 */
enum PlayerColor
{
	BLACK, WHITE
};

/**
 * Funkcja wypisujaca kod w zaleznosci od wartosci
 */
inline const char* toString(PlayerColor c)
{
	switch (c)
	{
	case BLACK:
		return "0";
	case WHITE:
		return "1";
	default:
		return "error";
	}
}

