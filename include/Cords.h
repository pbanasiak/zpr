#pragma once
#include <string>
#include <iostream>
#include <sstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;

/**
 * Klasa obslugujaca wspolrzedne
 */
class Cords
{
public:
	/**
	 * Konstruktor
	 * @param newX wspolrzedna X
	 * @param newY wspolrzedna Y
	 */
	Cords(int newX = -1, int newY = -1);
	/**
	 * Destruktor
	 */
	~Cords();

	int x, y;
	/**
	 * toString wypisujacy wspolrzedne
	 */
	std::string message();
	/**
	 * Szuka wspolrzednych na ktore moze skoczyc pionek po wykonaniu ewntualnego bicia
	 */
	std::vector<Cords> getPlaceAfterJump();
	/**
	 * Szuka wspolrzednych na ktore moze skoczyc damka po wykonaniu ewntualnego bicia
	 */
	std::vector<Cords> getPlaceAfterJumpKing();
	/**
	 * Operator porownania
	 */
	bool operator==(const Cords&);
	/**
	 * Zwraca odleglosc miedzy wspolrzednymi
	 * @param end punkt koncowy
	 */
	int distance(Cords);
	/**
	 * Zwraca pionki, ktore znajduja sie pomiedzy dwoma wspolrzednymi
	 * @param end wspolrzedna koncowa
	 */
	std::vector<Cords> between(Cords end);
private:
};

