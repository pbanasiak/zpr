#pragma once
#include "Cords.h"
#include "PlayerColor.h"
#include "JSONPiece.h"
#include <string>

/**
 * Klasa pionka, po niej dziedziczy rowniez klasa damki
 */
class Piece
{
public:
	/**
	 * Konstruktor
	 * @param newX wspolrzedna x
	 * @param newY wspolrzedna y
	 * @param c kolor damki
	 */
	Piece(int, int, PlayerColor);
	/**
	 * Destruktor
	 */
	virtual ~Piece()
	{
	}
	/**
	 * Kolor pionka
	 */
	PlayerColor color;
	/**
	 * Wspolrzedne pionka
	 */
	Cords cords;
	/**
	 * @return JSON dla pionka
	 */
	virtual JSONPiece getJSON();
	/**
	 * @return dla pionkow zwraca false, dla damek true
	 */
	virtual bool isKing();
	/**
	 * Zmiena wspolrzedne pionka
	 * @param c wspolrzedne na ktore ma zmienic
	 */
	void move(Cords);
	/**
	 * Sprawdza czy zmienic na damke
	 */
	bool isPromoting();
private:

};

