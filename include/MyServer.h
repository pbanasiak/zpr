#pragma once
#define _WEBSOCKETPP_CPP11_STL_
#define _WEBSOCKETPP_NO_CPP11_REGEX_
#include <set>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include <string.h>
#include <stdio.h>
#include "Model.h"
#include <sstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <websocketpp/common/thread.hpp>
#include <boost/log/core.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/trivial.hpp>
using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;
using websocketpp::lib::thread;
using websocketpp::lib::mutex;
using websocketpp::lib::unique_lock;
using websocketpp::lib::condition_variable;

typedef websocketpp::server<websocketpp::config::asio> server;

using websocketpp::connection_hdl;
using websocketpp::lib::placeholders::_1;

using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

enum action_type
{
	CONNECTED, UNCONNECTED, MESSAGE
};

/**
 * Klasa mapujaca komunikaty na akcje
 */
struct action
{
	/**
	 * Samo polaczenie
	 */
	action(action_type t, connection_hdl h) :
			type(t), hdl(h)
	{
	}
	/**
	 * Konstruktor dla polaczenia i wiadomosci
	 */
	action(action_type t, connection_hdl h, server::message_ptr m) :
			type(t), hdl(h), msg(m)
	{
	}

	action_type type;
	connection_hdl hdl;
	server::message_ptr msg;
};

class MyServer
{
private:
	/**
	 * Lista polaczen
	 */
	typedef std::set<connection_hdl, std::owner_less<connection_hdl>> con_list;
public:
	/**
	 * Konstruktor. Mapowanie handlerow eventow socketowych. I ustawianie logow socketow.
	 */
	MyServer();
	/**
	 * Handler polaczenia
	 * @param hdl polaczenie
	 */
	void on_open(connection_hdl);
	/**
	 * Handler zamykania polaczenia
	 * @param hdl polaczenie
	 */
	void on_close(connection_hdl);
	/**
	 * Hanlder do obslugi wiadomosci
	 * @param hdl polaczenie
	 * @param msg przesylana wiadomosc
	 */
	void on_message(connection_hdl, server::message_ptr);
	/**
	 * Funkcja dzialajacego serwera nasluchujaca na porcie
	 * @param port port
	 */
	void run(uint16_t port);
	/**
	 * Wysyla plansze do klienta
	 * @param room pokoj
	 */
	void sendBoard(int);
	/**
	 * Wyciaga wiadomosc i obsluguje ja
	 */
	void process_message();
	/**
	 * Obsluga struktury JSON
	 * @param hdl polaczenie
	 * @param msg
	 */
	void process_json(connection_hdl, server::message_ptr&);
	/**
	 * Tworzy nowy pokoj
	 */
	void createRoom();
	/**
	 * Obsluga klikniecia
	 * @param pt2 czesc JSON
	 */
	void parseClick(ptree& pt2);
	/**
	 * Wysyla liste pokoi wraz z liczba osob w nich
	 */
	void parseList();
	/**
	 * Obsluga chatu
	 */
	void parseChat(ptree& pt2);
	/**
	 * Przetwarzanie polaczenia
	 * @param hdl polaczenie
	 */
	const char* parseAdress(connection_hdl hdl);
	/**
	 * Przetwarzanie imion graczy
	 * @param name nick
	 * @param room numer pokoju
	 * @param hdl polaczenie
	 */
	void parseNicks(std::string, int, connection_hdl);
	/**
	 * Wysla komunikat do odpowiedniego pokoku
	 * @param text komunikat
	 * @param room numer pokoju
	 */
	void sendCommunicate(std::string, int);
	/**
	 * Zwraca liczbe graczy w pokoju
	 * @param room numer pokoju
	 * @return liczba graczy
	 */
	int howManyPlayers(int);

	/**
	 * koniec gry w pokoju
	 * @param room numer pokoju
	 * @param winner kto wygral
	 */
	void endGame(int, int);
private:
	/**
	 * Serwer
	 */
	server m_server;
	/**
	 * Lista polaczen
	 */
	con_list m_connections;
	/**
	 * Wektor modeli dla kazdego pokoju
	 */
	std::vector<Model*> rooms;
	/**
	 * Zbior imion uzytkownikow
	 */
	std::set<std::string> nicks;
	/**
	 * Kolejka akcji
	 */
	std::queue<action> m_actions;
	/**
	 * Przyporzadkowanie imion uzytkownika do odpowiedniego pokoju
	 */
	std::map<const char*, int> playersToRoom;
	/**
	 * Mutex na akcje
	 */
	mutex m_action_lock;
	/**
	 * Mutex na polaczenie
	 */
	mutex m_connection_lock;
	/**
	 * Zmienna warunkowa w akcjach
	 */
	condition_variable m_action_cond;
};

