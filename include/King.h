#include "Piece.h"

/**
 * Klasa damki
 */
class King: public Piece
{
public:
	/**
	 * @return JSON dla damki
	 */
	virtual JSONPiece getJSON();
	/**
	 * Konstruktor
	 * @param newX wspolrzedna x
	 * @param newY wspolrzedna y
	 * @param c kolor damki
	 */
	King(int newX, int newY, PlayerColor c);
	/**
	 * @return zawsze true, bo jest damka
	 */
	virtual bool isKing();

	/**
	 * Destruktor
	 */
	virtual ~King()
	{
	}
};
