CC=g++ 
CFLAGS=-c -Wall -std=c++11 
INCLUDE_PATH=-Iinclude -Iinclude/websocketpp
LINKER_PATH=
VPATH=src
OUTPUT_PATH=Release
BIN_PATH=$(OUTPUT_PATH)/bin
LDFLAGS= -lboost_log -lboost_thread -lpthread -lboost_regex -lboost_system
SOURCES= MyServer.cpp Model.cpp Piece.cpp Cords.cpp King.cpp
OBJECTS=$(SOURCES:.cpp=.o)
OBJ=$(foreach tmp,$(OBJECTS),$(OUTPUT_PATH)/bin/$(tmp))
EXECUTABLE=warcaby.out

all: dirs $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LINKER_PATH) $(OBJ) $(LDFLAGS) -o $(OUTPUT_PATH)/$@

.cpp.o:
	$(CC) $(INCLUDE_PATH) $(CFLAGS) $< -o $(OUTPUT_PATH)/bin/$@
	
dirs:
	mkdir -p $(OUTPUT_PATH)
	mkdir -p $(OUTPUT_PATH)/bin

clean: 
	rm -rf $(OBJ) $(OUTPUT_PATH)/$(EXECUTABLE)

doxygen:
	doxygen src/config

.PHONY: test
test:
	mkdir -p test/build
	rm -rf test/build/*
	cd test/build && cmake ..
	cd test/build && make
