#include "Piece.h"
#include "gtest/gtest.h"

using namespace std;
TEST(PieceTest, Constructor) {
	//Cords c(5,5);
	Piece p(5,5, WHITE);
	EXPECT_EQ(true, p.color == WHITE);
}

TEST(PieceTest, JSONTest) {
	Piece p(5, 6, WHITE);
	JSONPiece j = p.getJSON();
	EXPECT_EQ(true, j.x == 5);
	EXPECT_EQ(true, j.y == 6);
	EXPECT_EQ(true, j.color == 'w');
	EXPECT_EQ(true, j.type == 'n');
}

TEST(PieceTest, MovementTest) {
	//before
	Piece p(5, 6, WHITE);
	Cords c(5,6);
	EXPECT_EQ(true, p.cords == c);
	
	//after
	Cords m(2,3);
	p.move(m);
	EXPECT_EQ(true, p.cords == m);
}

TEST(PieceTest, PromotionTest) {
	Piece p(5,6, WHITE);
	EXPECT_EQ(false, p.isPromoting());
	Cords c(5, 0);
	p.move(c);
	EXPECT_EQ(true, p.isPromoting());
}
