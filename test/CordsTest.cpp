#include "Cords.h"
#include "gtest/gtest.h"

using namespace std;
TEST(CordsTest, Constructor) {
  Cords c(5,5);
  EXPECT_EQ(5, c.x);
  EXPECT_EQ(5, c.y);
}

TEST(CordsTest, CompareCords) {
	Cords c(5,5);
	Cords d(5,5);
	EXPECT_EQ(true, c == d);
	
	Cords e(6,6);
	EXPECT_EQ(false, c == e);
}

TEST(CordsTest, CountDistance) {
	Cords c(5,5);
	Cords d(6,6);
	
	EXPECT_EQ(1, c.distance(d));
	
	Cords e(1,4);
	EXPECT_EQ(-1, c.distance(e));
}

TEST(CordsTest, IsBetween) {
	Cords c(5,5);
	Cords d(6,6);
	
	vector<Cords> b = c.between(d);
	EXPECT_EQ(true, b.empty());
	
	Cords e(7,7);
	vector<Cords> v = c.between(e);
	Cords x = v.front();
	EXPECT_EQ(true, x == d);
	
	Cords q(2, 2);
	v = c.between(q);
	Cords x1 = v.front();
	Cords c2(3,3);
	Cords c3(4,4);
	EXPECT_EQ(true, x1 == c3);
	Cords x2 = v.back();
	EXPECT_EQ(true, x2 == c2);
}

TEST(CordsTest, CheckingFieldAfterAttack) {
	Cords c(5,5);
	vector<Cords> v = c.getPlaceAfterJump();
	Cords c3(3,3);
	EXPECT_EQ(true, c3 == (v.front()));
}

TEST(CordsTest, CheckingFieldAfterAttackForKing) {
	Cords c(5,5);
	vector<Cords> v = c.getPlaceAfterJumpKing();
	Cords c3(0,0);
	EXPECT_EQ(true, c3 == (v.front()));
}
