cmake_minimum_required(VERSION 2.6)

project(sample1)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
enable_testing()
find_package (Threads)
include_directories(../gtest-1.7.0/include)
include_directories(../include)
link_directories(/mybuild)

add_executable(testCords ../src/Cords.cpp CordsTest.cpp)
target_link_libraries(testCords gtest gtest_main)
target_link_libraries(testCords ${CMAKE_THREAD_LIBS_INIT})

add_executable(testPiece ../src/Cords.cpp ../src/Piece.cpp PieceTest.cpp)
target_link_libraries(testPiece gtest gtest_main)
target_link_libraries(testPiece ${CMAKE_THREAD_LIBS_INIT})

add_executable(testModel ../src/Cords.cpp ../src/Piece.cpp ../src/King.cpp ../src/Model.cpp ModelTest.cpp)
target_link_libraries(testModel gtest gtest_main)
target_link_libraries(testModel ${CMAKE_THREAD_LIBS_INIT})

add_test(NAME sample1 COMMAND sample1)
