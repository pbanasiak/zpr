#include "Model.h"
#include "gtest/gtest.h"

using namespace std;
TEST(ModelTest, Constructor) {
	Model m;
	int i = 0;
	for(Piece* p : m.getPieces())
		i++;
	EXPECT_EQ(16, i);
}

TEST(ModelTest, Find) {
	Model m;
	Cords c(3, 0);
	int r = m.find(c);
	EXPECT_EQ(3, r);
}

TEST(ModelTest, Move) {
	Model m;
	Cords c(4, 4);
	m.move(*m.getPieces()[3], c);
	EXPECT_EQ(m.getPieces()[3]->cords.x, c.x);
	EXPECT_EQ(m.getPieces()[3]->cords.y, c.y);

}
